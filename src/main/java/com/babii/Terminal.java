package com.babii;

import java.util.Scanner;
import static com.babii.FibonacciSolution.*;

/**
 * @author  Taras Babii
 * @version 1.0
 * @since   2019-03-30
 */
final class Terminal {

    /**
     * This is a utility class so
     * a constructor should be private.
     */
    private Terminal() {
    }

    /**
     * This method contains all required
     * methods to use the program by a user.
     */
    static void startTerminal() {
        Scanner scanner = new Scanner(System.in);
        intervalInitialization();
        listsInitialization();

        while (true) {
            System.out.println("Press 1 to print odd and even numbers");
            System.out.println("Press 2 to print the sum of "
                    + "odd and even numbers");
            System.out.println("Press 3 to build Fibonacci numbers");
            System.out.println("Press 4 to print percentage"
                    + " of odd and even Fibonacci numbers");
            System.out.println("Press 0 to exit");
            String terminalNumber = scanner.next();

            if (terminalNumber.equals("1")) {
                printNumbers();
            } else if (terminalNumber.equals("2")) {
                printSumOfNumbers();
            } else if (terminalNumber.equals("3")) {
                buildFibonacci();
            } else if (terminalNumber.equals("4")) {
                percentageFibonacci();
            } else if (terminalNumber.equals("0")) {
                System.exit(0);
            } else {
                System.out.println("You pressed an illegal number, try again");
            }

            System.out.println("\n");

        }

    }


}
