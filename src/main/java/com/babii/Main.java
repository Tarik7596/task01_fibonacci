package com.babii;

/**
 * @author  Taras Babii
 * @version 1.0
 * @since   2019-03-30
 */
public final class Main {

    /**
     * This is a utility class so
     * a constructor should be private.
     */
    private Main() {
    }

    /**
     * This is the main method which starts the program.
     * @param args Unused.
     */
    public static void main(final String[] args) {

        Terminal.startTerminal();

    }


}
