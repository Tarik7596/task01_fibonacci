package com.babii;

import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * @author  Taras Babii
 * @version 1.0
 * @since   2019-03-30
 */
final class FibonacciSolution {

    private static Scanner scanner = new Scanner(System.in);
    private static final float PERCENTAGE = 100.0f;
    private static int beginningOfInterval;
    private static int endOfInterval;
    private static LinkedList<Integer> oddNumbers = new LinkedList<Integer>();
    private static LinkedList<Integer> evenNumbers = new LinkedList<Integer>();
    private static LinkedList<Integer> fibonacciNumbers;

    /**
     * This is a utility class so
     * a constructor should be private.
     */
    private FibonacciSolution() {
    }

    /**
     * This method is used to initialize the interval
     * by a user.
     */
    static void intervalInitialization() {
        try {
            System.out.println("Enter the beginning of the interval:");
            beginningOfInterval = scanner.nextInt();
            System.out.println("Enter the end of the interval:");
            endOfInterval = scanner.nextInt();
            if (beginningOfInterval >= endOfInterval || beginningOfInterval <= 0
                    || endOfInterval <= 0) {
                throw new InputMismatchException();
            }
        } catch (InputMismatchException e) {
            System.out.println("You entered an illegal interval, try again");
            scanner.nextLine();
            intervalInitialization();
        }

    }

    /**
     * This method is used to initialize two lists.
     * The first one with odd numbers and
     * the second one with even numbers.
     */
    static void listsInitialization() {
        for (int currentNumber = beginningOfInterval;
             currentNumber <= endOfInterval;
             currentNumber++) {
            if (currentNumber % 2 == 0) {
                evenNumbers.addFirst(currentNumber);
            } else {
                oddNumbers.addLast(currentNumber);
            }
        }

    }

    /**
     * This method is used to print two
     * lists for a user.
     */
    static void printNumbers() {
        System.out.println("List of odd numbers:");
        System.out.println(oddNumbers);
        System.out.println("List of even numbers:");
        System.out.println(evenNumbers);

    }

    /**
     * This method is used for calculating
     * and printing the sum of both
     * even and odd numbers.
     */
    static void printSumOfNumbers() {
        int oddNumbersSum = 0;
        int evenNumbersSum = 0;

        for (int currentNumber : oddNumbers) {
            oddNumbersSum += currentNumber;
        }
        for (int currentNumber : evenNumbers) {
            evenNumbersSum += currentNumber;
        }

        System.out.println("The sum of odd numbers: " + oddNumbersSum);
        System.out.println("The sum of even numbers: " + evenNumbersSum);

    }

    /**
     * This method allows a user
     * to enter the size of set.
     * @return This returns the size number.
     */
    private static int setSizeInitialization() {
        int sizeOfSet;
        while (true) {
            try {
                System.out.println("Enter the size of set:");
                sizeOfSet = scanner.nextInt();
                if (sizeOfSet < 0) {
                    throw new InputMismatchException();
                }
                break;
            } catch (InputMismatchException e) {
                System.out.println("You entered an illegal number, try again");
                scanner.nextLine();
            }
        }
        return sizeOfSet;
    }

    /**
     * This method builds Fibonacci sequence
     * which size depends on a user choice.
     */
    static void buildFibonacci() {
        int sizeOfSet = setSizeInitialization();
        int previous = oddNumbers.getLast();
        int current = evenNumbers.getFirst();
        int next;
        fibonacciNumbers = new LinkedList<Integer>();

        fibonacciNumbers.addLast(previous);
        fibonacciNumbers.addLast(current);

        for (int i = 1; i <= sizeOfSet; i++) {
            next = current + previous;
            previous = current;
            current = next;
            fibonacciNumbers.addLast(next);
        }
        System.out.println("List of Fibonacci numbers:");
        System.out.println(fibonacciNumbers);

    }

    /**
     * This method prints a percentage
     * of odd and even numbers in Fibonacci
     * sequence for a user.
     */
    static void percentageFibonacci() {
        float oddCounter = 0.0f;
        float evenCounter = 0.0f;

        for (int i : fibonacciNumbers) {
            if (i % 2 == 0) {
                evenCounter++;
            } else {
                oddCounter++;
            }
        }

        float oddPercentage = (oddCounter / fibonacciNumbers.size())
                * PERCENTAGE;
        float evenPercentage = (evenCounter / fibonacciNumbers.size())
                * PERCENTAGE;

        System.out.println("Percentage of odd Fibonacci numbers: "
                + oddPercentage);
        System.out.println("Percentage of even Fibonacci numbers: "
                + evenPercentage);

    }



}
